import React from 'react';
import './App.css';
import logo from './assets/Logo.png';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} alt ="logo" width="20%"/>
        <p>
          Welcome to the Passion Maker App!!
        </p>
      </header>
    </div>
  );
}

export default App;
